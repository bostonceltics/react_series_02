import { useState } from "react";
import Box from "./Box";
import Field from "./Field";

function App() {
  const localColor = JSON.parse(localStorage.getItem("localColor"));
  const defaultcolor = "Empty value";

  let argsColor;
  localColor ? (argsColor = localColor) : (argsColor = defaultcolor);
  const [color, setColor] = useState(argsColor);
  const [hexValue, setHexValue] = useState();
  const [isDarkText, setIsDarkText] = useState(true);

  const setAndSaveColor = (newColor) => {
    setColor(newColor);
    localStorage.setItem("localColor", JSON.stringify(newColor));
  };

  return (
    <div className="App">
      <Box color={color} hexValue={hexValue} isDarkText={isDarkText} />
      <Field
        color={color}
        setAndSaveColor={setAndSaveColor}
        setHexValue={setHexValue}
        isDarkText={isDarkText}
        setIsDarkText={setIsDarkText}
      />
    </div>
  );
}

export default App;
