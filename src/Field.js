import colorNames from "colornames";

function Field({
  color,
  setAndSaveColor,
  setHexValue,
  isDarkText,
  setIsDarkText,
}) {
  return (
    <form className="field" onSubmit={(e) => e.preventDefault()}>
      <input
        autoFocus
        id="field"
        type="text"
        placeholder="Color for background"
        value={color === "Empty value" ? "" : color}
        onChange={(e) => {
          setAndSaveColor(e.target.value);
          setHexValue(colorNames(e.target.value));
        }}
      />
      <button type="button" onClick={() => setIsDarkText(!isDarkText)}>
        Iverse color
      </button>
    </form>
  );
}

export default Field;
