function Box({ color, hexValue, isDarkText }) {
  return (
    <div
      className="box"
      style={{ backgroundColor: color, color: isDarkText ? "#000" : "#fff" }}
    >
      <p>{color}</p>
      <p>{hexValue ? hexValue : null}</p>
    </div>
  );
}

export default Box;
